public class Problem4 {
	public static void main (String[] args) {
		int largest = 0;
		for (int i = 0; i < 1000; i++)
			for (int j = 0; j < 1000; j++) {
				int num = i * j;
				if (isPalindromic(num) && num > largest)
					largest = num;
			}
		System.out.println(largest);
	}

	public static boolean isPalindromic (int num) {
		String str = "" + num;
		for (int i = 0; i < str.length()/2; i++)
			if (str.charAt(0+i) != str.charAt(str.length()-1-i))
				return false;
		return true;
	}
}

