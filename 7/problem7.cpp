// Half-arsed refactoring of Problem 10
#include <cstdlib>
#include <iostream>
#include <cmath>
#define SEQ_PRIME_NUMBER 10001

using namespace std;

bool isPrime (unsigned int);

int main ()
{
	int primecount = 0;
	unsigned long testNumber = 2;
	while (primecount < SEQ_PRIME_NUMBER) {
		if (isPrime (testNumber)) {
			primecount++;
		}
		testNumber++;
	}

	cout << "Result: " << (testNumber - 1) << endl;

	return EXIT_SUCCESS;
}

bool isPrime (unsigned int i)
{
	if (i <= 1) return false;
	unsigned int upper = sqrt (i) + 1;
	for (unsigned int lower = 2; lower < upper; lower++) {
		if (i % lower == 0)
			return false;
	}
	return true;
}
