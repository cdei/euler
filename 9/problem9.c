#include <stdio.h>
#include <math.h>
// This limit is abitrarily big. 
// It can be optimised to 375 and still find the correct answer.
#define LIMIT 1000

int isTriplet (int, int, int);

int main (int argc, char* argv[])
{
	int a, b, c;
	for (b = LIMIT; b > 0; b--)
		for (a = b - 1; a > 0; a--) {
			c = sqrt(pow(a, 2) + pow(b, 2));
			if (a+b+c == 1000 && isTriplet(a,b,c)) {
				printf("%d", a * b * c);
				return 0;
			}
		}

	return 1;
}

int isTriplet (int a, int b, int c)
{
	return ((a < b) && (b < c) && (pow(a,2) + pow(b,2) == pow(c,2)));
}
