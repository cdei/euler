#include <stdio.h>
#include <math.h>
#define MAXIMUM_NUMBER 100

double sumbetween (int, int);

int main () {
	double r1 = sumbetween(MAXIMUM_NUMBER, 1);
	double r2 = sumbetween(MAXIMUM_NUMBER, 0);
	r2 = r2 * r2;
	printf ("%.0f\n", fabs(r1-r2));
	return 0;
}

double sumbetween (int i, int squared) {
	double sum = 0;
	for (; i > 0; i--) {
		if (squared)
			sum += (i * i);
		else
			sum += i;
	}
	return sum;
}
