#!/usr/bin/env ruby
def sumOfDigits(num)
	if num < 10
		return num
	else
		return (num % 10) + sumOfDigits(num / 10)
	end
end
puts sumOfDigits(2**1000);
