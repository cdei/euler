#include <stdio.h>

long collatz (long);

int main (int argc, char* argv[])
{
	long i, largestChain = -1, largest = -1;
	for (i = 1; i < 1000000; i++) {
		long chain = collatz(i);
		if (chain > largestChain) {
			largestChain = chain;
			largest = i;
		}
	}
	printf("%ld (%ld)\n", largest, largestChain);
	return 0;
}

long collatz (long num)
{
	if (num == 1) return 1;
	if (num % 2 == 0)
		return collatz(num / 2) + 1;
	else
		return collatz((3 * num) + 1) + 1;
}
