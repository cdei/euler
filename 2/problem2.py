#!/usr/bin/env python

def fibs(range):
  """ Seeded with the numbers given in the problem description """
  sequence = [1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
  while sequence[-1] < range:
    sequence.append(sequence[-1] + sequence[-2])
  return sequence;

fibSequence = fibs(4000000)
sum = 0
for num in fibSequence:
  if num % 2 == 0:
    sum += num

print sum

