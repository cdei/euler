#include <stdio.h>

int main (int argc, char* argv[])
{
	int seq[] = { 2, 3, 5 };
	int sum = 2;
	while (seq[2] <= 4000000) {
		int i;
		for (i = 1; i <= 2; i++) seq[i-1] = seq[i];
		seq[2] = seq[0] + seq[1];
		if (seq[2] % 2 == 0) sum += seq[2];
	}
	printf("%d\n", sum);
	return 0;
}
