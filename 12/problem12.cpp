#include <cstdio>
#include <cmath>

int divisors (int);

int main (int argc, char* argv[])
{
	int i, last;
	for (i = last = 1; divisors(i) <= 500; i += ++last);
	printf("%d\n", i);
}

int divisors (int num)
{
	int upper = sqrt(num), sum, i;
	for (i = 1, sum = 0; i < upper; i++)
		if (num % i == 0)
			sum++;
	return sum * 2;
}

