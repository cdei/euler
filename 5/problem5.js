var i = 0, even = false;
while (!even) {
	i++;
	even = evenlyDivides(i, 20);
}
console.log(i);

function evenlyDivides(num, den) {
	if (den == 0) return true;
	if (num % den == 0)
		return evenlyDivides(num, den-1);
	else
		return false;
}
