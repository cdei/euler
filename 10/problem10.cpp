#include <cstdlib>
#include <iostream>
#include <cmath>
#define MAXIMUM_NUMBER 2000000

using namespace std;

bool isPrime (unsigned int);

int main ()
{
	unsigned long long sum = 0;
	for (unsigned int i = 2; i < MAXIMUM_NUMBER; i++) {
		if (isPrime (i))
			sum += i;
	}
	cout << "Sum of primes: " << sum << endl;

	return EXIT_SUCCESS;
}

bool isPrime (unsigned int i)
{
	if (i <= 1) return false;
	unsigned int upper = sqrt (i) + 1;
	for (unsigned int lower = 2; lower < upper; lower++) {
		if (i % lower == 0)
			return false;
	}
	return true;
}
