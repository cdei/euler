#include <cmath>
#include <iostream>
#define TARGET 600851475143ULL
using namespace std;

bool isPrime (long long);

int main ()
{
	long long i, largest = -1, upper = sqrt(TARGET);
	for (i = upper; largest == -1 && i > 0; i--)
		if (TARGET % i == 0 && isPrime(i))
			largest = i;

	cout << largest << endl;

	return 0;
}

bool isPrime (long long i)
{
	if (i <= 1) return false;
	long long upper = sqrt (i) + 1;
	for (long long lower = 2; lower < upper; lower++) {
		if (i % lower == 0)
			return false;
	}
	return true;
}
